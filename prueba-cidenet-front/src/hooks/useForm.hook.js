import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
  createEmployee,
  resetValues,
  getOneEmployee,
  updateEmployee
} from "../redux/actions/employee.action";
import {
  validateText,
  notRequriedField,
  validateSelect,
  validateUID,
  validateDate,
} from "../utils";

const initialState = {
  firstName: "",
  secondName: "",
  first_lastName: "",
  second_lastName: "",
  typeId: "",
  uId: "",
  country: "",
  admissionDate: "",
  jobArea: "",
};
export const useForm = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const [form, setForm] = useState(initialState);
  const [errors, setErrors] = useState(initialState);
  const { employee } = useSelector((state) => state.employee);

  useEffect(() => {
    if (params.id) dispatch(getOneEmployee(params.id));
    return () => {
      dispatch(resetValues());
      setForm(initialState);
    };
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (Object.keys(employee).length) {
      setForm(employee);
    }
  }, [employee]);

  useEffect(() => {
    const values = Object.values(errors).every(
      (data) => data.length === 0 && data !== ""
    );
    if (values && params.id) {
      dispatch(updateEmployee(params.id, form));
    } else if (values) {
      dispatch(createEmployee(form));
    }
    // eslint-disable-next-line
  }, [errors]);

  const handleSubmit = (e) => {
    e.preventDefault();
    validateAllForm();
  };

  const validateAllForm = () => {
    setErrors({
      firstName: validateText(20)(form.firstName),
      secondName: notRequriedField(50)(form.secondName),
      first_lastName: validateText(20)(form.first_lastName),
      second_lastName: validateText(20)(form.second_lastName),
      typeId: validateSelect()(form.typeId),
      uId: validateUID(20)(form.uId),
      country: validateSelect()(form.country),
      admissionDate: validateDate()(form.admissionDate),
      jobArea: validateSelect()(form.jobArea),
    });
  };

  const onChange = (e) => {
    const { value, name } = e.target;
    setForm({ ...form, [name]: value.toUpperCase() });
  };

  const handleInput = (name) => ({
    name,
    value: form[name],
    onChange,
  });

  return { handleInput, handleSubmit, errors, setForm };
};
