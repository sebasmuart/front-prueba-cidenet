export const updateEmployeeService = async (axios, id, data) => {
    return await axios.patch(`/${id}`, data);
 }