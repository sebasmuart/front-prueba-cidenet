import { getEmployees } from "../../services/getEmployees";
import { createEmployeeService } from "../../services/createEmployees";
import { deleteEmployeeService } from "../../services/deleteEmployee";
import { getOneEmployeeService } from "../../services/getOneEmployee";
import { updateEmployeeService } from "../../services/updateEmployee";
import { searchService } from "../../services/searchService";
import { employeeTypes } from "../types/employee.types";
import { getEmailDomain } from "../../utils";
import { alertToDelete, alertError, alertSuccess } from "../../lib/third-party";

export const getAllEmployees = (pageSize = 10, pageNum = 1) => async (
  dispatch,
  getState
) => {
  const { axios } = getState().employee;
  try {
    const employee = await getEmployees(axios, pageSize, pageNum);
    if (employee.status === 200) {
      dispatch({
        type: employeeTypes.GET_EMPLOYEES,
        payload: { employees: employee.employees, count: employee.count },
      });
    }
  } catch (error) {
    console.log(error);
    dispatch({ type: employeeTypes.ERROR_EMPLYEES });
    alertError();
  }
};

export const searchByValue = (payload) => async (dispatch, getState) => {
  const { axios } = getState().employee;
  try {
    const employee = await searchService(axios, payload);
    console.log(employee);
    if (employee.status === 200) {
      dispatch({
        type: employeeTypes.GET_EMPLOYEES,
        payload: { employees: employee.employees.data },
      });
    }
  } catch (error) {
    console.log(error);
    dispatch({ type: employeeTypes.ERROR_EMPLYEES });
    alertError();
  }
};

export const createEmployee = (payload) => async (dispatch, getState) => {
  const { axios } = getState().employee;
  dispatch({ type: employeeTypes.REGISTER_LOADING, payload: true });
  try {
    const domain = getEmailDomain(payload);
    payload.email = `${payload.firstName}.${payload.first_lastName}@${domain}`;
    await createEmployeeService(axios, payload);
    dispatch({ type: employeeTypes.REGISTER_LOADING, payload: false });
    alertSuccess();
  } catch (error) {
    console.log("Error al crear el empleado", error);
    alertError();
    dispatch({ type: employeeTypes.REGISTER_LOADING, payload: false });
  }
};

export const deleteEmployee = (id) => async (dispatch, getState) => {
  const { axios } = getState().employee;
  try {
    const result = await alertToDelete();
    if (result.isConfirmed) {
      deleteEmployeeService(axios, id);
      dispatch({ type: employeeTypes.DELETE_EMPLOYEE, payload: id });
    }
  } catch (error) {
    console.log("Error al eliminar el empleado", error);
    alertError();
  }
};

export const getOneEmployee = (id) => async (dispatch, getState) => {
  const { axios } = getState().employee;
  const employee = await getOneEmployeeService(axios, id);
  if (employee.status === 200) {
    dispatch({
      type: employeeTypes.GET_ONE_EMPLOYEE,
      payload: employee.employees,
    });
  }
};

export const updateEmployee = (id, data) => async (dispatch, getState) => {
  const { axios } = getState().employee;
  dispatch({ type: employeeTypes.REGISTER_LOADING, payload: true });
  try {
    const domain = getEmailDomain(data);
    data.email = `${data.firstName}.${data.first_lastName}@${domain}`;
    await updateEmployeeService(axios, id, data);
    dispatch({ type: employeeTypes.REGISTER_LOADING, payload: false });
    alertSuccess();
  } catch (error) {
    console.log("Error al crear el empleado", error);
    alertError();
    dispatch({ type: employeeTypes.REGISTER_LOADING, payload: false });
  }
};

export const resetValues = () => async (dispatch) => {
  dispatch({
    type: employeeTypes.RESET_VALUES,
  });
};
