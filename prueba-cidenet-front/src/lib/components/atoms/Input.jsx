import React from "react";
import "../../../assets/scss/components/input.scss";

export const Input = ({ value, onChange, name, className, placeholder }) => {
  return (
    <input
      type="text"
      value={value}
      onChange={onChange}
      name={name}
      className={`${className}`}
      placeholder={placeholder}
    />
  );
};

Input.defaultProps = {
  placeholder: "",
}
