const apiResponse = response => {
   console.log(response);
   return {
      employees: response.data.employees,
      count: response.data.totalEmployees,
      status: response.status,
      statusText: response.statusText,
   }
}

export const getEmployees = async (axios, pageSize, pageNum) => {
   return apiResponse(await axios.get(`/all?pageSize=${pageSize}&pageNum=${pageNum}`));
}