import { Validator } from "./validator";

export const validateText = (max) => {
  return function (value) {
    const validatorText = new Validator(value);
    return validatorText
      .isNotEmpty("Este campo es requerido")
      .isLength(0, max, `El nombre debe de contener al menos ${max} caracteres`)
      .notSpecialCharacter("No son permitidos caracteres especiales").result;
  };
};

export const notRequriedField = (max) => {
  return function (value) {
    if (value) {
      const validatorText = new Validator(value);
      return validatorText
        .isLength(
          0,
          max,
          `El nombre debe de contener al menos ${max} caracteres`
        )
        .notSpecialCharacter("No son permitidos caracteres especiales").result;
    } else return [];
  };
};

export const validateSelect = () => {
  return function (value) {
    const validatorText = new Validator(value);
    return validatorText.isNotEmpty("Este campo es requerido").result;
  };
};

export const validateUID = () => {
  return function (value) {
    const validatorText = new Validator(value);
    return validatorText
      .isNotEmpty("Este campo es requerido")
      .isUID(
        "No se permiten caracteres espciales, ni al inicio ni al final. Sólo guiones en el medio"
      ).result;
  };
};

export const validateDate = () => {
  return function (value) {
    const validatorText = new Validator(value);
    return validatorText
      .isNotEmpty("Este campo es requerido")
      .isDate("Ingrese una fecha válida")
      .maxDate("No puede selecciar una fecha posterior a la de hoy").result;
  };
};
