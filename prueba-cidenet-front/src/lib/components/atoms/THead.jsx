import React from "react";

export const THead = () => {
  return (
    <thead>
      <tr className="header-table">
        <th>Nombre</th>
        <th>Segundo Nombre</th>
        <th>Primer Apellido</th>
        <th>Segundo Apellido</th>
        <th>Estado</th>
        <th>País</th>
        <th>Correo</th>
        <th>Tipo Documento</th>
        <th>Nº Documento</th>
        <th>Area</th>
        <th>Fecha Ingreso</th>
        <th>Fecha Registro</th>
        <th>Acciones</th>
      </tr>
    </thead>
  );
};
