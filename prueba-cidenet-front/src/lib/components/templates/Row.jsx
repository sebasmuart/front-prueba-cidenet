import React from "react";
import "../../../assets/scss/components/row.scss";

export const Row = ({ children, className }) => {
  return <div className={`inputs-container ${className}`}>{children}</div>;
};

Row.defaultProps = {
  className: "",
};
