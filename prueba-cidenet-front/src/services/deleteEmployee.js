export const deleteEmployeeService = async (axios, id) => {
    return await axios.delete(`/${id}`);
 }