
export function getEmailDomain(payload) {
  let domain = "CIDENET.COM.CO";
  if (payload.country === "USA") domain = "CIDENET.COM.US";
  return domain;
}

