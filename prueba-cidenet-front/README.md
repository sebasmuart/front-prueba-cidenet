# Descripción del Proyecto como Prueba Técnica

Este proyecto responde a los requerimientos de la prueba técnica de cidenet. En ningún momento pretende ser llevado más allá que las pruebas locales o de pruebas por parte de la empresa. Este correponde al front-end del aplicativo.

### Tech
El proyecto del lado del front end usa diferentes librerías para su funcionamiento

* [ReactJS](https://reactjs.com) - Librería de Facebook para el desarrollo del FrontEnd
* [Redux](https://es.redux.js.org/) - Gestor de Estados del aplicativo
* [Redux-thunk](https://github.com/reduxjs/redux-thunk) - Middleware para Redux
* [Node.js](https://nodejs.org/es/) - Entorno de desarrollo
* [ReactIcons](https://react-icons.github.io/react-icons/) - Libería de repositorio de íconos
* [SweetAlert2](https://sweetalert2.github.io/) - Libería para manejo de alertas
* [Sass](https://sass-lang.com/) - Preprocesador de CSS


# Instalación

El proyecto requiere [Node.js](https://nodejs.org/es/) v4+ para correr. Perferiblemente la versión LTS actual.

### Instalacción de dependencias mencionadas

```sh
$ cd prueba-cidenet-front
$ npm install 
```
### Levantar proyecto local
```sh
$ npm start
```

### Construir bundle del proyecto
```sh
$ npm build
```

### Pruebas Unitarias
```sh
$ npm test
```

## Consideraciones varias sobre la realización del proyecto
 - Se incluye ciertas pruebas unitarias, aunque me hubiese gustado implementar más por cuestiones de tiempo me fue imposible. Sin embargo lo implementado puede servir para evidenciar el conocimiento.
 
 - Para la Maquetación web con HTML y CSS, decidí no utilizar ninguna librería UI. Ya que queria simular ciertas necesidades de muchos proyectos, donde ciertos componentes o elementos es necesario construirlos de forma manual para tener el control total de sus estilos y comportamiento a la medida.
 
 - El proyecto front end responde a una simplificación del Sistema de Diseño conocido como [Atomic Design](https://www.uifrommars.com/atomic-design-ventajas/) para implemenzar la base de arquitectura del proyecto de front end y de la implementación de elementos UI. 



