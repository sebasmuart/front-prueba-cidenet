import * as React from "react";
import { Logo } from "../../../lib/components/atoms";
import { shallow, mount } from "enzyme";

describe("<Logo />", () => {
  it("Should be mounted", () => {
    const logo = shallow(<Logo />);
    expect(logo.length).toEqual(1);
  });

  it("Should has the image parame by default", () => {
      const logo = mount(<Logo image={""}/>);
      expect(logo.find('img').prop('src')).toEqual("");
  })
});
