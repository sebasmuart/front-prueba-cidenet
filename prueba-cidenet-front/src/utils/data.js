export const selecCountry = [
  {
    value: "COL",
    select: "COLOMBIA",
  },
  {
    value: "USA",
    select: "ESTADOS UNIDOS",
  },
];

export const typeOfId = [
  {
    value: "CC",
    select: "CÉDULA DE CIUDADANÍA",
  },
  {
    value: "CE",
    select: "CÉDULA DE EXTRANGERÍA",
  },
  {
    value: "PAS",
    select: "PASAPORTE",
  },
  {
    value: "PE",
    select: "PERMISO ESPECIAL",
  },
];

export const jobAreas = [
  {
    value: "ADMINISTRACION",
    select: "ADMINISTRACIÓN",
  },
  {
    value: "FINANCIERA",
    select: "FINANCIERA",
  },
  {
    value: "COMPRAS",
    select: "COMPRAS",
  },
  {
    value: "INFRAESTRUCTURA",
    select: "INFRAESTRUCTURA",
  },
  {
    value: "OPERACION",
    select: "OPERACIÓN",
  },
  {
    value: "TALENTO HUMANO",
    select: "TALENTO HUMANO",
  },
  {
    value: "SERVICIOS VARIOS",
    select: "SERVICIOS VARIOS",
  },
];
