/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import "../../../assets/scss/components/dataTable.scss";

//Components
import { Input } from "../atoms";
import { Table, FooterTable } from "../molecules";

//Actions
import {
  getAllEmployees,
  searchByValue,
} from "../../../redux/actions/employee.action";

export const DataTable = () => {
  const dispatch = useDispatch();
  const [valueSearch, setValueSearch] = useState({ search: "" });
  const { employees } = useSelector((state) => state.employee);

  useEffect(() => {
    dispatch(getAllEmployees());
  }, []);

  const handleSearch = (e) => {
    let { value } = e.target;
    setValueSearch({ search: value.toUpperCase() });
    if (value.length === 0) {
      dispatch(getAllEmployees());
    } else {
      dispatch(searchByValue(value.toUpperCase()));
    }
  };

  return (
    <div className="data-table-container">
      <div className="data-table-header">
        <h1>Tabla de Empleados</h1>
        <Input
          className="input-search"
          onChange={handleSearch}
          placeholder="Buscar por coincidencias..."
          value={valueSearch.search}
        />
      </div>
      <div className="data-table-body">
        <Table data={employees} />
      </div>
      <div className="data-table-footer">
        <FooterTable />
      </div>
    </div>
  );
};
