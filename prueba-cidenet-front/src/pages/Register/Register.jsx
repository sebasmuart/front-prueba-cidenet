import React from "react";
import { Form } from "../../lib/components/organisms";

export const Register = () => {
  return (
    <div className="main-container">
      <Form />
    </div>
  );
};
