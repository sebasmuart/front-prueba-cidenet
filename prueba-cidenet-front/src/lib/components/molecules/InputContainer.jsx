import React from "react";
import "../../../assets/scss/components/input.scss";

export const InputContainer = ({ children, className }) => {
  return <div className={className}>{children}</div>;
};

InputContainer.defaultProps = {
  className: "input-form-collection",
};
