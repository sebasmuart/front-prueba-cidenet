/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../../../assets/scss/components/footer.scss";
import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from "react-icons/md";
import { useSelector, useDispatch } from "react-redux";

//Actions
import { getAllEmployees } from "../../../redux/actions/employee.action";
const tokens = {
  size: "3rem",
  color: "#455BD9",
  cursor: "pointer",
};
export const FooterTable = () => {
  const dispatch = useDispatch();
  const { count } = useSelector((state) => state.employee);
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [tokensIconsRigth, setTokensIconsRigth] = useState(tokens);
  const [tokensIconsLeft, setTokensIconsLeft] = useState(tokens);

  useEffect(() => {
    if (pageNum === 1) {
      setTokensIconsLeft({
        ...tokensIconsLeft,
        color: "#9E9E9E",
        cursor: "default",
      });
    } else {
      setTokensIconsLeft(tokens);
    }

    if (pageNum === Math.ceil(count / 10)) {
      setTokensIconsRigth({
        ...tokensIconsRigth,
        color: "#9E9E9E",
        cursor: "default",
      });
    } else {
      setTokensIconsRigth(tokens);
    }
  }, [pageNum]);

  const nextEmployees = () => {
    if (pageNum < Math.ceil(count / 10)) {
      const _pageNum = pageNum + 1;
      dispatch(getAllEmployees(pageSize, _pageNum));
      setPageNum(pageNum + 1);
    }
  };

  const prevEmployees = () => {
    if (pageNum > 1) {
      const _pageNum = pageNum - 1;
      dispatch(getAllEmployees(pageSize, _pageNum));
      setPageNum(pageNum - 1);
    }
  };
  return (
    <div className="footer-actions">
      <div className="info-pagination">
        <p className="text-pages">{`Página ${pageNum} de ${Math.ceil(
          count / 10
        )}`}</p>
      </div>
      <div className="footer-action__icons">
        <span onClick={prevEmployees}>
          {" "}
          <MdKeyboardArrowLeft {...tokensIconsLeft} />
        </span>
        <span onClick={nextEmployees}>
          <MdKeyboardArrowRight {...tokensIconsRigth} />
        </span>
      </div>
    </div>
  );
};
