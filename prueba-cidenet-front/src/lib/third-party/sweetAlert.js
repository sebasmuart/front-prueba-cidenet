import Swal from "sweetalert2";

export const alertToDelete = async () => {
  return await Swal.fire({
    title: "¿Está seguro que desea eliminar a este empleado?",
    text: "No podrá revertir esta acción",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "green",
    cancelButtonColor: "#d33",
    confirmButtonText: "Eliminar",
    cancelButtonText: "Cancelar",
  });
};

export const alertSuccess = () => {
  Swal.fire({
    position: "center",
    icon: "success",
    title: "Los cambios se han guardado correctamente",
    showConfirmButton: false,
    timer: 1500,
  });
};

export const alertError = async () => {
  Swal.fire({
    icon: "error",
    title: "Oops...",
    text: "Algo salió mal. Comunícate con el administrador",
  });
};
