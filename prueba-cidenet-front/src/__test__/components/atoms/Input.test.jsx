import * as React from "react";
import { Input } from "../../../lib/components/atoms";
import { shallow, mount } from "enzyme";

describe("<Input />", () => {
  it("Should be mounted", () => {
    const logo = shallow(<Input />);
    expect(logo.length).toEqual(1);
  });

  it("Should has the image parame by default", () => {
    const onChangeMock = jest.fn();
    const logo = mount(<Input value="testValue" onChange={onChangeMock} />);
    expect(logo.props().value).toEqual("testValue");
  });

  it("Should has the tag input", () => {
    const logo = mount(<Input />);
    expect(logo.find("input").length).toEqual(1);
  });

  it("Should work the onChange attribute", () => {
    const onChangeMock = jest.fn();
    const logo = shallow(
      <Input onChange={onChangeMock} />
    );
    logo.find("input").simulate("change", "testValue");
    expect(onChangeMock).toBeCalledWith("testValue");
  });
});
