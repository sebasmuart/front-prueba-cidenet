import React from "react";
import "../../../assets/scss/components/input.scss";

export const Select = ({
  options,
  name,
  defaultValue,
  onChange,
  className,
  value,
  placeholder,
}) => {
  return (
    <select name={name} className={className} onChange={onChange} value={value}>
      <option value="">{defaultValue}</option>
      {options.map((option, index) => (
        <option value={option.value} key={index}>{option.select}</option>
      ))}
    </select>
  );
};

Select.defaultProps = {
  className: "select-form",
  defaultValue: "--Ninguno--"
};
