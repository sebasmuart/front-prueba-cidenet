import React from "react";
import { DataTable } from "../../lib/components/organisms";

export const Home = () => {
  return (
    <main className="main-container">
      <DataTable />
    </main>
  );
};
