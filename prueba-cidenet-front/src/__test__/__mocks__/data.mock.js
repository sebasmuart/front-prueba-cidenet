export const dataTableMock = [
  {
    id: 100,
    name: "Sebastián",
    second_name: "Alejandro",
    first_lastName: "Vallejo",
    second_lasname: "Rojas",
    state: true,
    country: "Colombia",
    email: "sebas.vallejo@correo.com",
    typeId: "CC",
    uid: "12340213AF",
    admissionDate: "17-12-2020",
    registerDate: "17-12-2020"
  }
];
