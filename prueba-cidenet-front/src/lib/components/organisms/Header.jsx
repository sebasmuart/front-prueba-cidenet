import React from "react";
import { Link } from "react-router-dom";
import logo from "../../../assets/static/cidenet-logo.png";
import "../../../assets/scss/components/header.scss";

export const Header = () => {
  return (
    <header className="header-container">
      <img src={logo} alt="hola mundo" />
      <ul className="nav-list">
        <li><Link to="/">Inicio</Link></li>
        <li><Link to="/register">Registrar Empleado</Link></li>
      </ul>
    </header>
  );
};
