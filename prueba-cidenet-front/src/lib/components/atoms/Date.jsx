import React, { useState } from "react";
import "../../../assets/scss/components/input.scss";

export const DateInput = ({
  value,
  onChange,
  name,
  className,
  placeholder,
}) => {
  const [type, setType] = useState("text");
  const onFocus = () => {
    setType("date");
  };
  const onBlur = () => {
    setType("text");
  };
  return (
    <input
      type={type}
      onFocus={onFocus}
      onBlur={onBlur}
      value={value}
      onChange={onChange}
      name={name}
      className={className}
      placeholder={placeholder}
      max={new Date()}
    />
  );
};

DateInput.defaultProps = {
  placeholder: "",
  className: "date-input-form",
};
