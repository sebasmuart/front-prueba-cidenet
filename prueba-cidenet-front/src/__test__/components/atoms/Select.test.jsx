import * as React from "react";
import { Select } from "../../../lib/components/atoms";
import { jobAreas } from "../../../utils/data";
import { shallow } from "enzyme";

describe("<Select />", () => {
  const select = shallow(<Select options={jobAreas} />);

  it("Should be mounted", () => {
    expect(select.length).toEqual(1);
  });

  it("Should receive the onChange", () => {
    const onChangeMock = jest.fn();
    const value = "testValue";
    const select = shallow(
      <Select options={jobAreas} onChange={onChangeMock} />
    );
    select.find("select").simulate("change", value);
    expect(onChangeMock).toBeCalledWith(value);
  });

  it("Should render options from prop options", () => {
    expect(select.find("option").length).toEqual(jobAreas.length + 1);
  });

});
