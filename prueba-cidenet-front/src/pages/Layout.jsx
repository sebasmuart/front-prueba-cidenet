import React from "react";
import { Header } from "../lib/components/organisms";

const Layout = ({ children }) => {
  return (
    <div className="App">
      <Header />
      {children}
    </div>
  );
};

export default Layout;
