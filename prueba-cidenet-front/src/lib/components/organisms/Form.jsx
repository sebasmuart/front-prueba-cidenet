/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "../../../assets/scss/components/form.scss";

//Initial Data
import { selecCountry, typeOfId, jobAreas } from "../../../utils/data";

//Components
import { Input, Select, DateInput, ErrorField } from "../atoms";
import { SubmitButton, InputContainer } from "../molecules";
import { Row } from "../templates";

//Custom Hooks
import { useForm } from "../../../hooks/";

export const Form = () => {
  const history = useHistory();
  const { created, isEdit } = useSelector((state) => state.employee);
  const { handleInput, handleSubmit, errors } = useForm();

  useEffect(() => {
    if (created) history.push("/");
  }, [created]);

  return (
    <form className="form-container" onSubmit={handleSubmit}>
      <h2>{isEdit ? "Actualizar datos Empleado" : "Registro de Empleados"}</h2>
      <div className="inputs">
        <Row>
          <InputContainer>
            {/*<--Input First  Name -->*/}
            <Input
              placeholder="Nombre del empleado"
              {...handleInput("firstName")}
              className={`input-form ${errors.firstName[0] && "error-input"}`}
            />
            {errors.firstName && <ErrorField msg={errors.firstName[0]} />}
          </InputContainer>
          <InputContainer>
            {/*<--Input Second Name -->*/}
            <Input
              placeholder="Segundo nombre"
              {...handleInput("secondName")}
              className={`input-form ${errors.secondName[0] && "error-input"}`}
            />
            {errors.secondName && <ErrorField msg={errors.secondName[0]} />}
          </InputContainer>
        </Row>
        <Row>
          <InputContainer>
            {/*<--Input First Last Name -->*/}
            <Input
              placeholder="Primer Apellido"
              {...handleInput("first_lastName")}
              className={`input-form ${
                errors.first_lastName[0] && "error-input"
              }`}
            />
            {errors.first_lastName && (
              <ErrorField msg={errors.first_lastName[0]} />
            )}
          </InputContainer>
          <InputContainer>
            {/*<--Input Second Last Name -->*/}
            <Input
              placeholder="Segundo Apellido"
              {...handleInput("second_lastName")}
              className={`input-form ${
                errors.second_lastName[0] && "error-input"
              }`}
            />
            {errors.second_lastName && (
              <ErrorField msg={errors.second_lastName[0]} />
            )}
          </InputContainer>
        </Row>
        <Row>
          <InputContainer>
            {/*<--Selec Type of UID -->*/}
            <Select
              options={typeOfId}
              defaultValue="Tipo de documento"
              {...handleInput("typeId")}
              className={`input-form ${errors.typeId[0] && "error-input"}`}
            />
            {errors.typeId && <ErrorField msg={errors.typeId[0]} />}
          </InputContainer>
          {/*<--Selec Country -->*/}
          <InputContainer>
            <Select
              options={selecCountry}
              defaultValue="País"
              {...handleInput("country")}
            />
            {errors.country && <ErrorField msg={errors.country[0]} />}
          </InputContainer>
        </Row>

        <Row>
          <InputContainer>
            {/*<--Input UID -->*/}
            <Input
              placeholder="Número de Identificación"
              {...handleInput("uId")}
              className={`input-form ${errors.uId[0] && "error-input"}`}
            />
            {errors.uId && <ErrorField msg={errors.uId[0]} />}
          </InputContainer>
          {/*<--Input Admission Date -->*/}
          <InputContainer>
            <DateInput
              placeholder="Fecha de Ingreso"
              {...handleInput("admissionDate")}
            />
            {errors.admissionDate && (
              <ErrorField msg={errors.admissionDate[0]} />
            )}
          </InputContainer>
        </Row>
        {/*<--Select Job Area -->*/}
        <Row>
          <InputContainer>
            <Select
              options={jobAreas}
              defaultValue="Área de trabajo"
              {...handleInput("jobArea")}
            />
            {errors.jobArea && <ErrorField msg={errors.jobArea[0]} />}
          </InputContainer>
        </Row>
      </div>
      <SubmitButton value={isEdit ? 'Actualizar' : 'Registrar'}/>
    </form>
  );
};
