/* eslint-disable import/no-anonymous-default-export */
import instance from "../../services";
import { employeeTypes } from "../types/employee.types";
const INITIAL_STATE = {
  axios: instance,
  employees: [],
  employee: {},
  employeesLength: 0,
  count: 0,
  loadingTable: true,
  loadingRegister: false,
  created: false,
  isEdit: false,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case employeeTypes.GET_EMPLOYEES:
      console.log(action.payload);
      return {
        ...state,
        employees: action.payload.employees,
        employeesLength: action.payload.employees.length,
        count: action.payload.count ? action.payload.count : state.count,
        loadingTable: false,
      };
    case employeeTypes.GET_ONE_EMPLOYEE:
      return {
        ...state,
        employee: action.payload,
        isEdit: true,
      }
    case employeeTypes.REGISTER_LOADING:
      return {
        ...state,
        loadingRegister: action.payload,
        created: !action.payload ? true : state.created,
      };
    case employeeTypes.RESET_VALUES:
      return {
        ...state,
        loadingTable: true,
        employee: {},
        loadingRegister: false,
        created: false,
        isEdit: false,
      };
    case employeeTypes.DELETE_EMPLOYEE:
      return {
        ...state,
        employees: state.employees.filter(data => data.id !== action.payload)
      }
    default:
      return state;
  }
}
