import * as React from "react";
import { DateInput } from "../../../lib/components/atoms";
import { shallow } from "enzyme";

describe("<Date />", () => {
  it("Should be mounted", () => {
    const logo = shallow(<DateInput />);
    expect(logo.length).toEqual(1);
  });

  it("Should be take the type date", () => {
    const logo = shallow(<DateInput />);
    expect(logo.prop('type')).toEqual('text');
    logo.simulate('focus');
    expect(logo.prop('type')).toEqual('date');
    logo.simulate('blur');
    expect(logo.prop('type')).toEqual('text');
  })

  it("Should has today as max day", () => {
    const logo = shallow(<DateInput />);
    expect(logo.prop('max')).toEqual(new Date());
  })

  it("Should work the onChange attribute", () => {
    const onChangeMock = jest.fn();
    const logo = shallow(
      <DateInput onChange={onChangeMock} />
    );
    logo.find("input").simulate("change", "testValue");
    expect(onChangeMock).toBeCalledWith("testValue");
  });

});