import React from "react";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import "../../../assets/scss/components/dataTable.scss";
import { FaEdit, FaTrash } from "react-icons/fa";

//Actions
import { deleteEmployee } from "../../../redux/actions/employee.action";

//Components
import { THead } from "../atoms";

export const Table = ({ data }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  // eslint-disable-next-line no-unused-vars
  const [tokensIcons, setTokensIcons] = useState({
    size: "1.5rem",
    color: "#455BD9",
    cursor: "pointer",
  });
  const [columns, setColumns] = useState([]);
  useEffect(() => {
    if (data && data.length) {
      const columns =
        data[0] && Object.keys(data[0]).filter((data) => data !== "id");
      setColumns(columns);
    }
  }, [data]);

  return (
    <table cellPadding={0} cellSpacing={0}>
      <THead />
      <tbody>
        {data.length ? (
          data.map((row) => (
            <tr>
              {columns.map((column) => (
                <td>{row[column]}</td>
              ))}
              <td className="action-table">
                <span onClick={() => history.push(`/register/${row.id}`)}>
                  <FaEdit {...tokensIcons} />
                </span>
                <span onClick={() => dispatch(deleteEmployee(row.id))}>
                  <FaTrash {...tokensIcons} />
                </span>
              </td>
            </tr>
          ))
        ) : (
          <p className="text-notexisting">No existen empleados</p>
        )}
      </tbody>
    </table>
  );
};
