import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Layout from "../pages/Layout";
import { Home } from "../pages/Home/Home";
import { Register } from "../pages/Register/Register";

const App = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/register/:id?" component={Register} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
};
export default App;
