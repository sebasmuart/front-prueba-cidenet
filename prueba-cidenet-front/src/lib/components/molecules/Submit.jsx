import React from "react";
import "../../../assets/scss/components/input.scss";

export const SubmitButton = ({
  value,
  className,
}) => {
  return <input className={className} type="submit" value={value} />;
};

SubmitButton.defaultProps = {
  className: "submit-form",
  value: "Registrar"
};
