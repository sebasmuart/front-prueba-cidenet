const apiResponse = (response) => {
  return {
    employees: response.data,
    status: response.status,
    statusText: response.statusText,
  };
};

export const searchService = async (axios, value) => {
  return apiResponse(await axios.get(`/search/${value}`));
};
