export class Validator {
  constructor(value) {
    this.value = value;
    this.result = [];
  }

  isNotEmpty(msg) {
    if (!this.value) {
      this.result.push(msg);
    }
    return this;
  }

  isLength(minLength, maxLength, msg) {
    if (this.value.length < minLength || this.value.length > maxLength) {
      this.result.push(msg);
    }
    return this;
  }

  notSpecialCharacter(msg) {
    if (!/^[A-Z]+(\s*[A-Z]*)*[A-Z]+$/.test(this.value)) {
      this.result.push(msg);
    }
    return this;
  }

  isUID(msg) {
    if (!/^[a-zA-Z0-9]+(\s*[a-zA-Z0-9-]*)*[a-zA-Z0-9]+$/.test(this.value)) {
      this.result.push(msg);
    }
    return this;
  }

  isDate(msg) {
    if (!/^\d{1,2}-\d{1,2}-\d{2,4}$/) {
      this.result.push(msg);
    }
    return this;
  }

  maxDate(msg) {
    if(new Date(this.value) > new Date()){
      this.result.push(msg);
    }
    return this;
  }

  isEmail(msg) {
    if (!/\S+@\S+\.\S+/.test(this.email)) {
      this.result.push(msg);
    }
    return this;
  }
}
