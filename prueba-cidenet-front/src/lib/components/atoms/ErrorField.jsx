import React from "react";
import "../../../assets/scss/components/input.scss"

export const ErrorField = ({ className, msg }) => {
  return <p className={className}>{msg}</p>;
};

ErrorField.defaultProps = {
  className: "input-error",
};
