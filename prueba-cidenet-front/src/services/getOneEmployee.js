const apiResponse = response => {
    return {
       employees: response.data,
       status: response.status,
       statusText: response.statusText,
    }
 }
 
 export const getOneEmployeeService = async (axios, id) => {
    return apiResponse(await axios.get(`/${id}`));
 }