import React from "react";
import logo from "../../../assets/static/cidenet-logo.png";

export const Logo = ({ image }) => {
  return (
    <figure>
      <img src={image} alt="Logo Cidenet" />
    </figure>
  );
};

Image.defaultProps = {
  image: logo,
};
